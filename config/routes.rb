Rails.application.routes.draw do
  devise_for :users
  root to: 'home#index'
  resources :users, only: [:show, :edit, :update]
  resources :posts, only: [:new, :create, :show, :destroy] do
  	resources :comments, only: [:index, :create], shallow: true
  end
end
