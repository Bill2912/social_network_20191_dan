class CommentsController < ApplicationController
	def index
    	@comments = @post.comments.includes(:user)
  	end
	def create
		#@comment = @post.comments.create(comment_params)
		@post = Post.find_by_id(params[:post_id])
		@comment = @post.comments.create(comment_params)
		if @comment.save
			redirect_to @post
		else
			flash.now[:danger] = "error"
		end
	end
	private
    def comment_params
    	params.required(:comment).permit :user_id, :post_id, :content
  	end

	#private
	#def set_post
	#	@post = User.posts.find(params[:post_id])
	#end
	#def set_user
	#	@user = User.find(params[:user_id])
	#end
	#def comment_params
	#	params[:comment].permit()
	#end
end
